import { test, expect } from '@playwright/test';

test('adding and deleting', async ({ page }) => {
  await page.goto('/');
  await page.getByPlaceholder('Add Tasks').click();
  await page.getByPlaceholder('Add Tasks').fill('Test task');
  await page.getByRole('button', { name: 'Add' }).click();
  await page.getByPlaceholder('Add Tasks').click();
  await page.getByPlaceholder('Add Tasks').fill('Second task');
  await page.getByRole('button', { name: 'Add' }).click();
  await page.getByRole('button', { name: '' }).first().click();
  await expect(page.getByText('Third task')).not.toBeVisible();
  await page.getByPlaceholder('Add Tasks').click();
  await page.getByPlaceholder('Add Tasks').fill('Third task');
  await page.getByRole('button', { name: 'Add' }).click();
  await expect(page.getByText('Third task')).toBeVisible();
});

test('completing', async({ page}) => {
  await page.goto('/');
  await page.getByPlaceholder('Add Tasks').fill('Complete task');
  await page.getByRole('button', { name: 'Add' }).click();
  await expect(page.getByText('Complete task')).toBeVisible();
  await page.getByRole('button', { name: 'Complete' }).click();
  await expect(page.getByText('Complete task')).not.toBeVisible();
})
